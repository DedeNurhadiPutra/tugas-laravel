<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $title  = 'List Pemain Film';
        // $casts  = DB::table('casts')->get();
        $casts  = Cast::all();

        return view('casts.index', compact('title', 'casts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $title  = 'Form Data Pemain Film Baru';

        return view('casts.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'nama'  => 'required',
            'umur'  => 'required|numeric',
            'bio'   => 'required'
        ]);

        // $query = DB::table('casts')->insert([
        //     'nama'  => $request['nama'],
        //     'umur'  => $request['umur'],
        //     'bio'  => $request['bio']
        // ]);

        $store = Cast::create([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast')->with('success', 'Data Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $title = 'Detail Pemain Film';
        // $cast = DB::table('casts')->where('id', $id)->first();
        $cast = Cast::find($id);

        return view('casts.detail', compact('title', 'cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $title = 'Edit Data Pemain Film';
        // $cast = DB::table('casts')->where('id', $id)->first();
        $cast = Cast::find($id);

        return view('casts.edit', compact('title', 'cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $request->validate([
            'nama'  => 'required',
            'umur'  => 'required|numeric',
            'bio'   => 'required'
        ]);

        // $query = DB::table('casts')
        //     ->where('id', $id)
        //     ->update([
        //         'nama'  => $request['nama'],
        //         'umur'  => $request['umur'],
        //         'bio'  => $request['bio']
        //     ]);

        $update = Cast::where('id', $id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast')->with('success', 'Berhasil Update Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // $query = DB::table('casts')->where('id', $id)->delete();
        Cast::destroy($id);

        return redirect('/cast')->with('success', 'Data Berhasil Dihapus');
    }
}
