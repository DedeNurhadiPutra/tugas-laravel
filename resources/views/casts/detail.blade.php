@extends('layouts.master')
@section('title', 'Detail Pemain Film')

@section('content')
<div class="container-fluid">
    <h4>{{$cast->nama}}</h4>
    <h6>{{$cast->umur}}</h6>
    <p>{{$cast->bio}}</p>
</div>
@endsection