@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">{{$title}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/cast" method="post">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="text" class="form-control" id="nama" name="nama" placeholder="Enter nama" value="{{ old('nama', '') }}">
              @error('nama')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="umur">Umur</label>
              <input type="number" class="form-control" id="umur" name="umur" placeholder="umur" value="{{ old('umur', '') }}">
              @error('umur')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="bio">Bio</label>
              <input type="text" class="form-control" id="bio" name="bio" placeholder="bio" value="{{ old('bio', '') }}">
              @error('bio')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
          </div>
        </form>
      </div>
      <!-- /.card -->
</div>
@endsection