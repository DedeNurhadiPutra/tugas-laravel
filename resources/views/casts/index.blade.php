@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">{{$title}}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                @endif
                <a href="/cast/create" class="btn btn-primary mb-2">Tambah Data Baru</a>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th style="width: 80px">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($casts as $key => $cast)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$cast->nama}}</td>
                            <td>{{$cast->umur}}</td>
                            <td>{{$cast->bio}}</td>
                            <td style="display: flex">
                                <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">Detail</a>
                                <a href="/cast/{{$cast->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                                <form action="/cast/{{$cast->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda yakin?')" value="delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3" align="center"> Data Tidak Ditemukan. </td>
                        </tr>
                    @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            {{-- <div class="card-footer clearfix">
              <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">«</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">»</a></li>
              </ul>
            </div> --}}
          </div>
    </div>
@endsection