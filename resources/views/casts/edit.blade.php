@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">{{"$title $cast->id"}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/cast/{{$cast->id}}" method="post">
            @csrf
            @method('PUT')
          <div class="card-body">
            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="text" class="form-control" id="nama" name="nama" placeholder="Enter nama" value="{{ old('nama', $cast->nama) }}">
              @error('nama')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="umur">Umur</label>
              <input type="text" class="form-control" id="umur" name="umur" placeholder="umur" value="{{ old('umur', $cast->umur) }}">
              @error('umur')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="bio">Bio</label>
              <input type="text" class="form-control" id="bio" name="bio" placeholder="bio" value="{{ old('bio', $cast->bio) }}">
              @error('bio')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
      <!-- /.card -->
</div>
@endsection